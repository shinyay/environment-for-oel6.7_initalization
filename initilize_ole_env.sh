#!/bin/sh
yum install -y vim
git config --global user.name "Shinya Yanagihara"
git config --global user.email "shinya.com@gmail.com"
git config --global core.editor 'vim -c "set fenc=utf-8"'
git config --global color.diff auto
git config --global color.status auto
git config --global color.branch auto

CHEFDK_URL=https://packages.chef.io/stable/el/6
CHEFDK_VER=chefdk-0.17.17-1.el6.x86_64.rpm
wget ${CHEFDK_URL}/${CHEFDK_VER}
rpm -ivh ${CHEFDK_VER}

echo 'eval "$(chef shell-init bash)"' >> ~/.bash_profile

echo 'export EDITOR=vim' >> ~/.bashrc
